﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit1.Lesson2
{
    class Program
    {
        public static ActorSystem MyActorSystem;

        static void Main(string[] args)
        {
            MyActorSystem = ActorSystem.Create("MyActorSystem");
            Console.WriteLine($"Actor system: '{MyActorSystem.Name}' created.");

            var consoleWriterActor = MyActorSystem.ActorOf(Props.Create(() => new ConsoleWriterActor()), "consoleWriterActor001");
            var consoleReaderActor = MyActorSystem.ActorOf(Props.Create(() => new ConsoleReaderActor(consoleWriterActor)), "consoleReaderActor001");

            Console.WriteLine($"actors '{nameof(consoleWriterActor)}' '{nameof(consoleReaderActor)}' created");
            // executing actor by sending him message
            consoleReaderActor.Tell(ConsoleReaderActor.StartCommand); // this is not some magic string

            // blocks the main thread from exiting until the actor system is shut down
            MyActorSystem.WhenTerminated.Wait();
        }
    }
}
