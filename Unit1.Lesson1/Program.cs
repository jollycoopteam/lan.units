﻿using Akka.Actor;
using System;

namespace Unit1.Lesson1
{
	class Program
	{
		public static ActorSystem MyActorSystem;

		static void Main(string[] args)
		{
			MyActorSystem = ActorSystem.Create("MyActorSystem");
			Console.WriteLine($"Actor system: '{MyActorSystem.Name}' created.");

			var consoleWriterActor = MyActorSystem.ActorOf(Props.Create(() => new ConsoleWriterActor()), "consoleWriterActor001");
			var consoleReaderActor = MyActorSystem.ActorOf(Props.Create(() => new ConsoleReaderActor(consoleWriterActor)), "consoleReaderActor001");
			Console.WriteLine($"actors '{nameof(consoleWriterActor)}' '{nameof(consoleReaderActor)}' created");
			// executing actor by sending him message
			consoleReaderActor.Tell("start"); // this is not some magic string

			// blocks the main thread from exiting until the actor system is shut down
			MyActorSystem.WhenTerminated.Wait();
		}
		private static void PrintInstructions()
		{
			Console.WriteLine("Write whatever you want into the console!");
			Console.Write("Some lines will appear as");
			Console.ForegroundColor = ConsoleColor.DarkRed;
			Console.Write(" red ");
			Console.ResetColor();
			Console.Write(" and others will appear as");
			Console.ForegroundColor = ConsoleColor.Green;
			Console.Write(" green! ");
			Console.ResetColor();
			Console.WriteLine();
			Console.WriteLine();
			Console.WriteLine("Type 'exit' to quit this application at any time.\n");
		}
	}
}
