﻿open Akka.FSharp
open System
open Akka.Actor

[<Literal>]
let Start = "start"

[<Literal>]
let Exit = "exit"

let display color msg = 
    Console.ForegroundColor <- color
    printfn "%A" msg

type Continue() = 
    class
    end

type InputSuccess(reason : string) = 
    member internal x.Reason = reason

type InputError(reason : string) = 
    member internal x.Reason = reason

type NullError(reason : string) = 
    inherit InputError(reason)

type ValidationError(reason : string) = 
    inherit InputError(reason)

type WriterActor() = 
    inherit Actor()
    override x.OnReceive message = 
        match message with
        | :? InputError as e -> display ConsoleColor.Red e.Reason
        | :? InputSuccess as s -> display ConsoleColor.Green s.Reason
        | x -> Console.WriteLine(x)
        Console.ResetColor()

type ReaderActor(writer : IActorRef) = 
    inherit Actor()
    member val _writer = writer
    
    member private x.PrintInstructions() = 
        printfn "Write whatever you want into the console!"
        printfn "Some entries will pass validation, and some won't...\n\n"
        printfn "Type 'exit' to quit this application at any time.\n"
    
    member private x.GetvalidateInput() = 
        let msg = Console.ReadLine()
        if (String.IsNullOrEmpty(msg)) then x.Self.Tell(NullError("Empty"))
        else if (String.Equals(msg, Exit, StringComparison.OrdinalIgnoreCase)) then 
            Actor.Context.System.Terminate() |> ignore
        else if (msg.Length % 2 = 0) then 
            x._writer.Tell(InputSuccess("Thank you! Message was valid."))
            x.Self.Tell(Continue())
        else x.Self.Tell(ValidationError("Invalid: input had odd number of characters."))
    
    override x.OnReceive message = 
        match message with
        | :? string as str when str.Equals(Start) -> x.PrintInstructions()
        | :? InputError as err -> x._writer.Tell(err)
        | _ -> ()
        x.GetvalidateInput()

[<EntryPoint>]
let main argv = 
    let system = ActorSystem.Create("MyActorSystem")
    let writer = system.ActorOf(Props.Create<WriterActor>(), "consoleWriterActor001")
    let reader = system.ActorOf(Props.Create<ReaderActor>(writer), "consoleReaderActor001")
    reader.Tell(Start)
    system.WhenTerminated.Wait()
    0
