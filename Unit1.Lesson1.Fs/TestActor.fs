﻿module TestActor
open Akka.FSharp
open System

let testActor (mailbox : Actor<'a>) (message : string) = 
    let isEven = message.Length % 2 = 0
    match isEven with
    | true -> printfn "Your string had an even number of characters.\n"
    | false -> printfn "Your string had an odd number of characters.\n"
