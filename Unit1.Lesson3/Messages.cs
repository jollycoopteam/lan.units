﻿namespace Unit1.Lesson3
{
    #region Neutral/system messages
    /// <summary>
    /// Marker class to continue processing.
    /// </summary>
    internal class ContinueProcessing
    {

    }
    #endregion

    #region Success messages
    /// <summary>
    /// Base class for signaling that user input was valid.
    /// </summary>
    internal class InputSuccess
    {
        internal string Reason { get; private set; }
        public InputSuccess(string reason)
        {
            Reason = reason;
        }
    }
    #endregion

    #region Error messages
    /// <summary>
    /// Base class for signaling that user input was invalid.
    /// </summary>
    internal class InputError
    {
        internal InputError(string reason)
        {
            Reason = reason;
        }

        internal string Reason { get; private set; }
    }

    /// <summary>
    /// User provided blank input.
    /// </summary>
    internal class NullInputError : InputError
    {
        internal NullInputError(string reason) : base(reason) { }
    }

    /// <summary>
    /// User provided invalid input (currently, input w/ odd # chars)
    /// </summary>
    internal class ValidationError : InputError
    {
        internal ValidationError(string reason) : base(reason) { }
    }
    #endregion
}
