﻿open Akka.FSharp
open System
open Akka.Actor

[<Literal>]
let Start = "start"

[<Literal>]
let Exit = "exit"

let display color msg = 
    Console.ForegroundColor <- color
    printfn "%A" msg

type Continue() = 
    class
    end

type InputSuccess(reason : string) = 
    member internal x.Reason = reason

type InputError(reason : string) = 
    member internal x.Reason = reason

type NullError(reason : string) = 
    inherit InputError(reason)

type ValidationError(reason : string) = 
    inherit InputError(reason)

type ValidationActor(writer : IActorRef) = 
    inherit Actor()
    member val _writer = writer
    override x.OnReceive message = 
        let msg = message :?> string
        match (String.IsNullOrEmpty(msg)) with
        | true -> x._writer.Tell(NullError("No input received."))
        | false -> 
            match msg.Length % 2 = 0 with
            | true -> x._writer.Tell(InputSuccess("Thank you! Message was valid."))
            | false -> x._writer.Tell(ValidationError("Invalid: input had odd number of characters."))
        x.Sender.Tell(Continue())

type WriterActor() = 
    inherit Actor()
    override x.OnReceive message = 
        match message with
        | :? InputError as e -> display ConsoleColor.Red e.Reason
        | :? InputSuccess as s -> display ConsoleColor.Green s.Reason
        | x -> Console.WriteLine(x)
        Console.ResetColor()

type ReaderActor(validator : IActorRef) = 
    inherit Actor()
    member val _validator = validator
    
    member private x.PrintInstructions() = 
        printfn "Write whatever you want into the console!"
        printfn "Some entries will pass validation, and some won't...\n\n"
        printfn "Type 'exit' to quit this application at any time.\n"
    
    member private x.GetvalidateInput() = 
        let msg = Console.ReadLine()
        match (not (String.IsNullOrEmpty(msg)) && String.Equals(msg, Exit, StringComparison.OrdinalIgnoreCase)) with
        | true -> Actor.Context.System.Terminate() |> ignore
        | false -> x._validator.Tell(msg)
    
    override x.OnReceive message = 
        if (message.Equals(Start)) then x.PrintInstructions()
        x.GetvalidateInput()

[<EntryPoint>]
let main argv = 
    let system = ActorSystem.Create("MyActorSystem")
    let writerProps = Props.Create<WriterActor>()
    let writerActor = system.ActorOf(writerProps, "writerActor")
    let validatorProps = Props.Create<ValidationActor>(writerActor)
    let validatorActor = system.ActorOf(validatorProps, "validatorActor")
    let readerProps = Props.Create<ReaderActor>(validatorActor)
    let readerActor = system.ActorOf(readerProps, "readerActor")
    readerActor.Tell(Start)
    system.WhenTerminated.Wait()
    0
