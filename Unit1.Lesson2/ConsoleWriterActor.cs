﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit1.Lesson2
{
    internal class ConsoleWriterActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            if (message is InputError)
            {
                var msg = message as InputError;
                DisplayMessage(msg, ConsoleColor.Red);
            }
            else if (message is InputSuccess)
            {
                var msg = message as InputSuccess;
                DisplayMessage(msg, ConsoleColor.Green);
            }
            else
            {
                Console.WriteLine(message);
            }

            Console.ResetColor();
        }

        private static void DisplayMessage(InputSuccess msg, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg.Reason);
        }

        private static void DisplayMessage(InputError msg, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg.Reason);
        }
    }
}
