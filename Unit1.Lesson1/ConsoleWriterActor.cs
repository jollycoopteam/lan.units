﻿using Akka.Actor;
using System;

namespace Unit1.Lesson1
{
	internal class ConsoleWriterActor : UntypedActor
	{
		protected override void OnReceive(object message)
		{
			var msg = message as string;

			// checking message contents
			if (string.IsNullOrWhiteSpace(msg))
			{
				DisplayFormatedOutput("Please provide some valid input. \n");
				return;
			}

			// if message has even # characters, display in red; else, green
			var even = msg.Length % 2 == 0;

			var color = even ? ConsoleColor.Red : ConsoleColor.Green;

			var alert = even 
				? "Your string had an even number of characters.\n" 
				: "Your string had an odd number of characters.\n";

			DisplayFormatedOutput(alert, color);
		}

		private void DisplayFormatedOutput(string v)
		{
			Console.ForegroundColor = ConsoleColor.DarkYellow;
			Console.WriteLine(v);
			Console.ResetColor();
		}
		private void DisplayFormatedOutput(string v, ConsoleColor c)
		{
			Console.ForegroundColor = c;
			Console.WriteLine(v);
			Console.ResetColor();
		}
	}
}
