﻿using Akka.Actor;

namespace Unit1.Lesson3
{
    /// <summary>
    /// 3rd lesson of Akka.net bootcamp course
    /// Info available at <a href="https://github.com/petabridge/akka-bootcamp/tree/master/src/Unit-1/lesson3">AKKA-bootcamp</a>
    /// </summary>
    internal class Program
    {
        static void Main(string[] args)
        {
            var MyActorSystem = ActorSystem.Create("MyActorSystem");

            var consoleWriterProps = Props.Create(() => new ConsoleWriterActor());
            var consoleWriterActor = MyActorSystem.ActorOf(consoleWriterProps, "consoleWriterActor");

            var validationActorProps = Props.Create(() => new ValidationActor(consoleWriterActor));
            var validationActor = MyActorSystem.ActorOf(validationActorProps, "validationActor");

            var consoleReaderProps = Props.Create<ConsoleReaderActor>(validationActor);
            var consoleReaderActor = MyActorSystem.ActorOf(consoleReaderProps, "consoleReaderActor");
            // tell console reader to begin
            consoleReaderActor.Tell(ConsoleReaderActor.StartCommand);

            // blocks the main thread from exiting until the actor system is shut down
            MyActorSystem.WhenTerminated.Wait();
        }
    }
}
