﻿#load "Bootstrap.fsx"

open System
open Akka.FSharp
open Akka.Actor

let writerActor (mailbox : Actor<'a>) (messsage : obj) = 
    let digest msg = 
        if (String.IsNullOrEmpty(msg)) then printfn "Please provide some valid input. \n"
        else 
            match msg.Length % 2 = 0 with
            | true -> printfn "Your string had an even number of characters.\n"
            | false -> printfn "Your string had an odd number of characters.\n"
    match messsage with
    | :? string as m -> digest m
    | _ -> printfn "Please provide some valid input. \n"

let readerActor (mailbox : Actor<'a>) (msg : obj) = 
    match Console.ReadLine() with
    | "exit" | "" | " " -> mailbox.Context.System.Terminate() |> ignore
    | m -> writerActor mailbox m
    mailbox.Self.Tell "continue"

let system = System.create "defualt-system" (Configuration.load())
let aref = spawn system "reader-actor" (actorOf2 readerActor)

aref.Tell("start")
system.WhenTerminated.Wait()
