﻿using System;
using Akka.Actor;

namespace Unit1.Lesson1
{
	/// <summary>
	/// Actor that is responsible for reading FROM the console
	/// Also responsible for calling <see cref="ActorSystem.Terminate">
	/// 
	/// When actor recieves messege OnRecieve is fired. Then actor starts listening to console input
	/// and if input is fired and is valid actor will send input as message to other actor: <see cref="ConsoleWriterActor">
	/// </summary>
	internal class ConsoleReaderActor : UntypedActor
	{
		const string ExitCommand = "exit";
		readonly IActorRef _consoleWriterActor;

		public ConsoleReaderActor(IActorRef consoleWriterActor)
		{
			_consoleWriterActor = consoleWriterActor;
		}


		protected override void OnReceive(object message)
		{
			var input = Console.ReadLine();
			if (!string.IsNullOrEmpty(input) && string.Equals(input, ExitCommand, StringComparison.OrdinalIgnoreCase))
			{
				// this will shut down the system
				//(acquire handle to system via
				// this actors context)
				Context.System.Terminate();
				return;
			}
			_consoleWriterActor.Tell(input);

			// continue reading messages from the console
			// this will probably fire OnRecieve again.
			Self.Tell("continue");
		}
	}
}
