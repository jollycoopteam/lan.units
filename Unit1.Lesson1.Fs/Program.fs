﻿open Akka.FSharp
open System
open Akka.Actor

type WriterActor() = 
    inherit Actor()
    override x.OnReceive message = 
        let check msg = 
            if (String.IsNullOrEmpty(msg)) then printfn "Please provide some valid input. \n"
            else 
                match msg.Length % 2 = 0 with
                | true -> printfn "Your string had an even number of characters.\n"
                | false -> printfn "Your string had an odd number of characters.\n"
        match message with
        | :? string as msg -> check msg
        | _ -> printfn "Please provide some valid input. \n"

type ReaderActor(writer : IActorRef) = 
    inherit Actor()
    member val _writer = writer
    override x.OnReceive message = 
        let input = Console.ReadLine()
        match input with
        | "exit" | "" | " " -> Actor.Context.System.Terminate() |> ignore
        | msg -> x._writer.Tell(msg)
        x.Self.Tell("continue")

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    let system = ActorSystem.Create("MyActorSystem")
    let writer = system.ActorOf(Props(typedefof<WriterActor>), "consoleWriterActor001")
    let reader = system.ActorOf(Props.Create(typedefof<ReaderActor>, writer), "consoleReaderActor001")
    reader.Tell("start")
    system.WhenTerminated.Wait()
    0
